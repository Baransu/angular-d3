import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DatePickerModule } from 'ng2-datepicker';
import { AppComponent } from './app.component';

import { StockChartComponent } from './chart/stock-chart.component';
import { DailyChartComponent } from './daily/daily-chart.component';
import { StatsComponent } from './stats/stats.component';

@NgModule({
  declarations: [ AppComponent, StockChartComponent, StatsComponent, DailyChartComponent ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DatePickerModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
