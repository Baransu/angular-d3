export class StockChartConfig {
  settings: { stroke: string, symbol: string, show: boolean, name: string };
  dataset: Array<{date: string, x: string, y: number, high: number, low: number, volume: number, open: number }>
}

export const color = (index: number) => {
  const colors = [index, index + 1, index + 2].map(seed => {
    const x = Math.sin(seed) * 10000
    return Math.floor((x - Math.floor(x)) * 255);
  })
  return `rgb(${colors.join()})`;
}
