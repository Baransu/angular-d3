import { Component, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import * as d3 from 'd3';
import { format } from 'date-fns';

import { StockChartConfig } from './stock-chart-config';

@Component({
  selector: 'stock-chart',
  templateUrl: './stock-chart.component.html',
  styleUrls: ['./stock-chart.component.css']
})
export class StockChartComponent {

  @Input() config: Array<StockChartConfig>;
  @Output() change: EventEmitter<any> = new EventEmitter();

  private selectedDate;
  private brush;

  private host;        // D3 object referencing host dom object
  private htmlElement: HTMLElement; // Host HTMLElement

  private svg;         // chart SVG
  private context;     // upper chart
  private focus;       // lower chart

  private margin;
  private margin2;

  private width;
  private height;
  private height2;

  private xScale;
  private xScale2;

  private yScale;
  private yScale2;

  private xAxis;
  private xAxis2;

  private yAxis;

  private line;

  private stocks: Array<StockChartConfig>;

  /* D3 wrapper around angular element */
  constructor(private element: ElementRef) {
    this.htmlElement = this.element.nativeElement;
    this.host = d3.select(this.element.nativeElement);
  }

  /* rebuilding chart on @Input change */
  ngOnChanges(): void {
    this.stocks = this.config.filter(c => c.settings.show);
    this.setup();
    this.buildSVG();
    this.populate();
    this.drawXAxis();
    this.drawYAxis();
  }

  onChange() {
    this.change.emit({ date: this.selectedDate, brush: this.brush, width: this.width });
  }

  /* setting up chart container values */
  private setup(): void {
    this.margin = { top: 10, right: 0, bottom: 40, left: 30 };
    this.margin2 = { top: 10, right: 0, bottom: 40, left: 30 };

    this.width = this.htmlElement.clientWidth - this.margin.left - this.margin.right;

    this.height = this.width * 0.5 - this.margin.top - this.margin.bottom;
    this.height2 = this.width * 0.25 - this.margin2.top - this.margin2.bottom;

    this.xScale = d3.scaleTime().range([0, this.width]);
    this.xScale2 = d3.scaleTime().range([0, this.width]);

    this.yScale = d3.scaleLinear().range([this.height, 0]);
    this.yScale2 = d3.scaleLinear().range([this.height2, 0]);

    this.line = d3.line()
        .x(d => this.xScale(d[0]))
        .y(d => this.yScale(d[1]))
  }

  private buildSVG(): void {
    this.host.html('');
    this.svg = this.host.append('svg')
      .attr('width', this.width + this.margin.left + this.margin.right)
      .attr('height',
            this.height + this.margin.top + this.margin.bottom +
            this.height2 + this.margin2.top + this.margin2.bottom
           )
      .append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    this.focus = this.svg.append('g')
      .attr("class", "focus")

    this.context = this.svg.append('g')
      .attr("class", "context")
      .attr(
        "transform",
        `translate(0, ${this.height + this.margin2.top + this.margin.bottom})`
      );

    this.svg.append("defs").append("clipPath")
      .attr("id", "clip")
      .append("rect")
      .attr("width", this.width)
      .attr("height", this.height);
  }

  private drawXAxis(): void {
    const tickFormat = t => format(t, 'DD MMM YYYY')
    this.xAxis = d3.axisBottom(this.xScale)
      .tickFormat(tickFormat)

    this.xAxis2 = d3.axisBottom(this.xScale2)
      .tickFormat(tickFormat)

    this.focus.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', `translate(0, ${this.height})`)
      .call(this.xAxis)

    this.context.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', `translate(0, ${this.height2})`)
      .call(this.xAxis2)
  }

  private drawYAxis(): void {
    this.yAxis = d3.axisLeft(this.yScale)

    this.focus.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr("y", 10)
      .attr("fill", "#000")
      .text("Price(USD)")
  }

  /* get max y value from dataset */
  private getMaxY(): number {
    const maxValues = this.stocks.map(data => Math.max(...data.dataset.map(d => d.y)));
    const max = Math.max(...maxValues)
    return max + max/10;
  }

  /* get max dataset x length */
  private getXLength(): number {
    const maxValues = this.stocks.map(data => {
      return data.dataset.length;
    });
    return Math.max(...maxValues)
  }

  /* Pushing data into chart */
  private populate(): void {
    if(this.stocks.length === 0) {
      return;
    }

    this.stocks.forEach(stock => {

      this.xScale.domain(d3.extent(stock.dataset, d => d.x))
      this.xScale2.domain(this.xScale.domain())

      this.yScale.domain([0, this.getMaxY()]);
      this.yScale2.domain(this.yScale.domain());

      /* converting object to tuple because of some type problems */
      const data = stock.dataset.map(d => ([d.x, d.y]))

      this.focus.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .on("click", (e) => {
          this.selectedDate = e[0]
          this.onChange();
        })
        .attr("x", d => this.xScale(d[0]))
        .attr("width", this.width/this.getXLength())
        .attr("height", this.height)

      this.focus.append('path')
        .datum(data)
        .attr('class', 'line')
        .style('fill', 'none')
        .style('stroke', stock.settings.stroke)
        .style('stroke-width', '1.5px')
        .style('clip-path', 'url(#clip)')
        .attr('d', this.line)

      const line2 = d3.line()
        .x(d => this.xScale2(d[0]))
        .y(d => this.yScale2(d[1]))

      this.context.append("path")
        .datum(data)
        .attr('class', 'line')
        .style('fill', 'none')
        .style('stroke', stock.settings.stroke)
        .style('stroke-width', '1.5px')
        .attr("d", line2)

    })

    const brushed = () => {
      const s = d3.event.selection || this.xScale2.range();
      this.brush = s;
      this.onChange();
      this.xScale.domain(s.map(this.xScale2.invert, this.xScale2));
      this.focus.selectAll(".line").attr("d", this.line);
      this.focus.selectAll(".bar").attr("x", d => this.xScale(d[0]));
      this.focus.select(".axis--x").call(this.xAxis);
    }

    const brush = d3.brushX()
      .extent([[0, 0], [this.width, this.height2]])
      .on("brush end", brushed);

    this.context.append('g')
      .attr("class", "brush")
      .call(brush)
      .call(brush.move, this.xScale2.range());
  }
}
