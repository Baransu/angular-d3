import { Component, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { StockChartConfig } from '../chart/stock-chart-config';
import * as d3 from 'd3';
import { format, differenceInDays } from 'date-fns';

@Component({
  selector: 'daily-chart',
  templateUrl: './daily-chart.component.html',
  styleUrls: ['./daily-chart.component.css']
})
export class DailyChartComponent {

  @Input() config: StockChartConfig;
  @Input() brush;

  private host;        // D3 object referebcing host dom object
  private htmlElement: HTMLElement; // Host HTMLElement

  private svg;
  private margin;
  private width;
  private height;
  private xScale;
  private yScale;
  private xAxis;
  private yAxis;

  private line;

  private stock = [];

  /* D3 wrapper around angular element */
  constructor(private element: ElementRef) {
    this.htmlElement = this.element.nativeElement;
    this.host = d3.select(this.element.nativeElement);
  }

  /* rebuilding chart on @Input change */
  ngOnChanges(): void {
    this.stock = this.config.dataset.filter((c, i) => i >= this.brush[1] && i <= this.brush[0])
    this.setup();
    this.buildSVG();
    this.populate();
    this.drawXAxis();
    this.drawYAxis();
  }

  /* setting up chart container values */
  private setup(): void {
    this.margin = { top: 10, right: 0, bottom: 40, left: 40 };

    this.width = this.htmlElement.clientWidth - this.margin.left - this.margin.right;
    this.height = this.width * 0.3 - this.margin.top - this.margin.bottom;

    this.xScale = d3.scaleTime().range([0, this.width]);
    this.yScale = d3.scaleLinear().range([this.height, 0]);

    this.line = d3.line()
        .x(d => this.xScale(d[0]))
        .y(d => this.yScale(d[1]))
  }

  private buildSVG(): void {
    this.host.html('');
    this.svg = this.host.append('svg')
      .attr('width', this.width + this.margin.left + this.margin.right)
      .attr('height', this.height + this.margin.top + this.margin.bottom)
      .append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    this.svg.append("defs").append("clipPath")
      .attr("id", "clip")
      .append("rect")
      .attr("width", this.width)
      .attr("height", this.height);
  }

  private drawXAxis(): void {
    const tickFormat = t => format(t, 'DD MMM YYYY')
    this.xAxis = d3.axisBottom(this.xScale)
      .tickFormat(tickFormat)

    this.svg.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', `translate(0, ${this.height})`)
      .call(this.xAxis)
  }

  private drawYAxis(): void {
    this.yAxis = d3.axisLeft(this.yScale)

    this.svg.append('g')
      .attr('class', 'axis axis--y')
      .call(this.yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr("y", 10)
      .attr("fill", "#000")
      .text("Price(USD)")
  }

  /* get max y value from dataset */
  private getMaxY(): number {
    const max = Math.max(...this.stock.map(d => d.y))
    return max + max/2;
  }

  /* get max dataset x length */
  private getXLength(): number {
    return this.config.dataset.length;
  }

  /* Pushing data into chart */
  private populate(): void {
    this.xScale.domain(d3.extent(this.stock, d => d.x))
    this.yScale.domain(d3.extent(this.stock, d => d.y));

    /* converting object to tuple because of some type problems */
    const data = this.stock.map(d => ([d.x, d.y, d.y - d.open]))

    this.svg.selectAll(".daily-bar")
      .data(data)
      .enter().append("rect")
      .attr("class", "daily-bar")
      .attr("fill", d => d[2] > 0 ? "green" : "red")
      .attr("x", d => this.xScale(d[0]))
      .attr("y", d => this.yScale(d[1]))
      .attr("width", this.width/this.getXLength())
      .attr("height", d => Math.abs(d[2]))
      .style('clip-path', 'url(#clip)')

    this.svg.append('path')
        .datum(data)
        .attr('class', 'line')
        .style('fill', 'none')
        .style('stroke', this.config.settings.stroke)
        .style('stroke-width', '1.5px')
        .style('clip-path', 'url(#clip)')
        .attr('d', this.line)
  }
}
