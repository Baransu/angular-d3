import { Component } from '@angular/core';

import { StatsService } from './stats.service';
import { StockChartConfig, color } from '../chart/stock-chart-config';

import { format, subMonths, isEqual } from 'date-fns'

interface Company {
  symbol: string,
  name: string
}

interface MyCompany {
  symbol: string,
  name: string,
  onChart: boolean
}

@Component({
  selector: 'stats-component',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [ StatsService ],
})
export class StatsComponent {
  public filter: string = '';
  public selectedSymbol: string;
  public endDate;
  public startDate;
  public endPickerOptions;
  public startPickerOptions;
  public stockChartConfig: Array<StockChartConfig>;
  public color = color;

  private companies: Array<Company> = [];
  private myCompanies: Array<MyCompany>;

  private startIndex: number = 0;
  private endIndex: number = 0;
  private dayDate;

  constructor(private statsService: StatsService) {
    this.stockChartConfig = new Array<StockChartConfig>();

    this.startPickerOptions = {
      maxDate: new Date(),
      initialDate: subMonths(Date.now(), 6)
    }

    this.endPickerOptions = {
      maxDate: new Date(),
      initialDate: new Date()
    }

    this.myCompanies = [
      { name: 'Tesla Motors, Inc', symbol: 'TSLA', onChart: true }
    ];

    this.selectedSymbol = "TSLA";
    this.getStock(this.myCompanies);
    this.getCompanies()
  }

  get brush() {
    return [ this.startIndex, this.endIndex ];
  }

  get filteredCompanies() {
    const filter = this.filter.toLowerCase();
    if(filter.length === 0) {
      return [];
    }
    return this.companies.filter((company, index) => {
      const inMyCompanies = this.myCompanies.find(c => c.symbol === company.symbol);
      return !inMyCompanies && (company.name.toLowerCase().startsWith(filter) || company.symbol.toLowerCase().startsWith(filter))
    }).filter((c, index) => index < 10);
  }

  get isLoading() {
    return this.filter.length > 0 && this.statsService.hasMore();
  }

  get daily() {
    if(!this.dayDate) {
      return [];
    };
    return this.stockChartConfig.filter(c => c.settings.show).map(c => {
      const stock = c.dataset.find(s => isEqual(s.x, this.dayDate));
      return Object.assign(stock, c.settings);
    })
  }

  get dayDateFormatted() {
    return format(this.dayDate, 'YYYY-MM-DD');
  }

  get activeCompanies() {
    return this.stockChartConfig.filter(c => c.settings.show);
  }

  get allStock() {
    const stock = this.stockChartConfig.find(s => s.settings.symbol === this.selectedSymbol)
    return stock ? stock.dataset.filter((c, i) => i >= this.endIndex && i <= this.startIndex) : [];
  }

  selectOnTable(symbol: string) {
    this.selectedSymbol = symbol;
  }

  onChange() {
    const start = this.startDate;
    const end = this.endDate;
    if(end && start && (end.formated !== this.statsService.end || start !== this.statsService.start )) {
      this.getStock(this.myCompanies);
    }
  }

  onChartChange(event) {
    if(this.stockChartConfig.length === 0) {
      return;
    }

    const { date, brush, width } = event;
    const start = brush[0]/width;
    const end = brush[1]/width;
    const len = this.stockChartConfig[0].dataset.length;
    this.startIndex = len - Math.floor(len * start)
    this.endIndex = len - Math.floor(len * end)
    this.dayDate = date;
  }

  addCompany(symbol: string) {
    const company = this.companies.find(c => c.symbol === symbol);
    const index = this.companies.indexOf(company);
    this.companies.splice(index, 1);
    this.myCompanies.push(Object.assign(company, { onChart: true }));
    this.filter = '';
    this.getStock(this.myCompanies);
  }

  toggleOnChart(i: number) {
    const company = this.myCompanies[i];
    company.onChart = !company.onChart;
    this.stockChartConfig = this.stockChartConfig.map(stock => {
      if(company.symbol === stock.settings.symbol) {
        stock.settings.show = !stock.settings.show;
      }
      return stock;
    })
  }

  getCompanies() {
    if(this.statsService.hasMore()) {
      this.statsService
        .getCompanies()
        .subscribe(stats => {
          stats = stats.json();
          const { industry } = stats.query.results;
          this.companies = this.companies.concat(industry.reduce((acc, a) => {
            const companies = Array.isArray(a.company) ? a.company : [a.company];
            return acc.concat(companies.filter(c => c && c.name && c.symbol));
          }, []))
          this.getCompanies();
        })
    }
  }

  getStock(companies: Array<MyCompany>) {
    if(!this.startDate || !this.endDate) {
      return;
    }
    companies = companies.filter(c => c.onChart);
    const symbols = companies.map(c => c.symbol);
    this.statsService
      .getStock(symbols, this.startDate, this.endDate)
      .subscribe(stats => {
        stats = stats.json();
        if(stats.query.results === null) {
          return;
        }

        const data = stats.query.results.quote

        this.stockChartConfig = new Array<StockChartConfig>();
        companies.forEach(({ symbol, name, onChart }, index) => {
          let stock = new StockChartConfig();
          stock.settings = {
            stroke: this.color(index),
            symbol,
            name,
            show: onChart
          };
          stock.dataset = data
            .filter(d => d.Symbol === symbol)
            .map(d => {
              return {
                x: new Date(d.Date),
                y: Number(d.Close),
                high: Number(d.High),
                low: Number(d.Low),
                open: Number(d.Open),
                date: format(d.Date, 'YYYY-MM-DD'),
                volume: Number(d.Volume)
              }
            })
          this.stockChartConfig.push(stock);
        })
      })
  }
}
