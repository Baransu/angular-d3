import { StatsService } from './stats.service';
import { format, subMonths } from 'date-fns';

describe('Service: StatsService', () => {

  it('should have correct dates', () => {
    const service = new StatsService();
    expect(service.end).toEqual(format(Date.now(), 'YYYY-MM-DD'))
    expect(service.start).toEqual(format(subMonths(Date.now(), 6), 'YYYY-MM-DD'))
  })

  it('should return true if more than 0 ids', () => {
    let service = new StatsService();
    expect(service.hasMore()).toBe(true)
    service.ids = [];
    expect(service.hasMore()).toBe(false)
  })
})
