import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { compareAsc, subMonths, format, isValid } from 'date-fns';

@Injectable()
export class StatsService {

  private ids: Array<number>;
  public start;
  public end;

  constructor(private http: Http) {
    // tslint:disable-next-line
    this.ids = [431,720,610,611,112,773,772,132,730,310,821,422,744,330,333,738,750,758,346,348,347,515,724,723,751,313,760,826,739,725,633,110,350,326,425,426,427,841,812,815,755,345,210,763,131,424,344,813,731,516,732,846,810,836,423,622,913,513,510,511,514,733,512,756,766,911,314,735,753,722,620,341,340,757,417,418,910,714,912,634,636,716,134,734,522,825,635,737,311,526,736,524,312,121,627,621,752,133,434,851,850,852,420,421,742,430,710,843,523,632,624,770,120,769,631,721,343,521,754,520,525,527,626,410,447,726,820,743,814,136,327,123,124,125,122,325,324,811,323,762,318,623,835,342,842,432,448,729,727,728,776,449,317,332,412,414,411,416,413,415,771,440,442,443,444,441,445,446,761,768,630,711,712,322,419,837,765,823,830,833,832,834,831,775,135,625,528,113,713,745,715,316,740,764,130,433,111,822,767,844,845,320,321,628,351,741,315,774,331,637,914,759,840,827,824]

    const now = Date.now();
    this.start = format(subMonths(now, 6), 'YYYY-MM-DD');
    this.end = format(now, 'YYYY-MM-DD');
  }

  hasMore(): boolean {
    return this.ids.length > 0;
  }

  getCompanies(): Observable<any> {
    const ids = this.ids.splice(0, 10);
    const allIds = ids.join('","');
    const query = encodeURIComponent(`select * from yahoo.finance.industry where id in ("${allIds}")`);
    // tslint:disable-next-line
    const url = `https://query.yahooapis.com/v1/public/yql?q=${query}&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=`

    return this.http.get(url);
  }

  getStock(companies: Array<string>, start, end): Observable<any> {
    if(isValid(start.momentObj._d)) {
      this.start = start.formatted;
    }

    if(isValid(end.momentObj._d)) {
      this.end = end.formatted;
    }

    if(compareAsc(start.momentObj._d, end.momentObj._d) === 1) {
      let tmp = this.start;
      this.start = this.end;
      this.end = tmp;
    }

    const symbols = companies.join('","');
    // tslint:disable-next-line
    const query = encodeURIComponent(`select * from yahoo.finance.historicaldata where symbol in ("${symbols}") and startDate = "${this.start}" and endDate = "${this.end}"`);
    // tslint:disable-next-line
    const url = `https://query.yahooapis.com/v1/public/yql?q=${query}&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=`;
    return this.http.get(url);
  }
}
